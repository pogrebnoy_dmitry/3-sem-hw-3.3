package org.sample;

public class PrefixScan extends Thread {
    int threadId;
    Pair[][] pairsTable;

    public PrefixScan(int threadId, Pair[][] pairsTable) {
        this.threadId = threadId;
        this.pairsTable = pairsTable;
    }

    public void run() {
        int depth = Main.LOG_COT;

        Pair pair = Main.pairs[threadId * Main.SECTION];
        for (int i = threadId * Main.SECTION + 1; i < (threadId + 1) * Main.SECTION; i++) {
            pair = pair.add(Main.pairs[i]);
        }
        pairsTable[depth][threadId] = pair;

        Main.wall.allSynchronized();

        prefix(depth);

        Main.wall.allSynchronized();
    }

    private void prefix(int depth) {
        if (depth == 0) {
            Main.wall.allSynchronized();
            return;
        }

        if (threadId < 1 << (depth - 1)) {
            pairsTable[depth - 1][threadId] = pairsTable[depth][2 * threadId].add(pairsTable[depth][2 * threadId + 1]);
        }
        Main.wall.allSynchronized();
        prefix(depth - 1);
        Main.wall.allSynchronized();
    }
}
