package org.sample;

public class Wall {
    private int maxThreadsEnd;
    private int curThreadsEnd;

    Wall(int n) {
        maxThreadsEnd = n;
        curThreadsEnd = 0;
    }

    synchronized void allSynchronized() {
        curThreadsEnd++;
        if (curThreadsEnd < maxThreadsEnd) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            notifyAll();
            curThreadsEnd = 0;
        }
    }
}