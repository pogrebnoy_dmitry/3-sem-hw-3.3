# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=UTF-8
# Warmup: 5 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.parallel_1_thread

# Run progress: 0,00% complete, ETA 00:12:30
# Fork: 1 of 1
# Warmup Iteration   1: 1,687 ms/op
# Warmup Iteration   2: 1,949 ms/op
# Warmup Iteration   3: 1,829 ms/op
# Warmup Iteration   4: 1,872 ms/op
# Warmup Iteration   5: 2,172 ms/op
Iteration   1: 2,077 ms/op
Iteration   2: 2,040 ms/op
Iteration   3: 2,046 ms/op
Iteration   4: 1,954 ms/op
Iteration   5: 2,138 ms/op
Iteration   6: 1,983 ms/op
Iteration   7: 1,707 ms/op
Iteration   8: 1,984 ms/op
Iteration   9: 1,947 ms/op
Iteration  10: 1,934 ms/op


Result "org.sample.MyBenchmark.parallel_1_thread":
  1,981 ±(99.9%) 0,175 ms/op [Average]
  (min, avg, max) = (1,707, 1,981, 2,138), stdev = 0,116
  CI (99.9%): [1,806, 2,156] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=UTF-8
# Warmup: 5 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.parallel_2_thread

# Run progress: 20,00% complete, ETA 00:10:02
# Fork: 1 of 1
# Warmup Iteration   1: 0,789 ms/op
# Warmup Iteration   2: 0,851 ms/op
# Warmup Iteration   3: 0,789 ms/op
# Warmup Iteration   4: 0,634 ms/op
# Warmup Iteration   5: 0,628 ms/op
Iteration   1: 0,633 ms/op
Iteration   2: 0,624 ms/op
Iteration   3: 0,606 ms/op
Iteration   4: 0,597 ms/op
Iteration   5: 0,604 ms/op
Iteration   6: 0,601 ms/op
Iteration   7: 0,616 ms/op
Iteration   8: 0,587 ms/op
Iteration   9: 0,605 ms/op
Iteration  10: 0,585 ms/op


Result "org.sample.MyBenchmark.parallel_2_thread":
  0,606 ±(99.9%) 0,023 ms/op [Average]
  (min, avg, max) = (0,585, 0,606, 0,633), stdev = 0,015
  CI (99.9%): [0,583, 0,629] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=UTF-8
# Warmup: 5 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.parallel_4_thread

# Run progress: 40,00% complete, ETA 00:07:32
# Fork: 1 of 1
# Warmup Iteration   1: 0,145 ms/op
# Warmup Iteration   2: 0,143 ms/op
# Warmup Iteration   3: 0,145 ms/op
# Warmup Iteration   4: 0,145 ms/op
# Warmup Iteration   5: 0,145 ms/op
Iteration   1: 0,145 ms/op
Iteration   2: 0,145 ms/op
Iteration   3: 0,145 ms/op
Iteration   4: 0,145 ms/op
Iteration   5: 0,144 ms/op
Iteration   6: 0,144 ms/op
Iteration   7: 0,146 ms/op
Iteration   8: 0,144 ms/op
Iteration   9: 0,144 ms/op
Iteration  10: 0,144 ms/op


Result "org.sample.MyBenchmark.parallel_4_thread":
  0,145 ±(99.9%) 0,001 ms/op [Average]
  (min, avg, max) = (0,144, 0,145, 0,146), stdev = 0,001
  CI (99.9%): [0,143, 0,146] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=UTF-8
# Warmup: 5 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.parallel_8_thread

# Run progress: 60,00% complete, ETA 00:05:01
# Fork: 1 of 1
# Warmup Iteration   1: 1,275 ms/op
# Warmup Iteration   2: 1,222 ms/op
# Warmup Iteration   3: 1,448 ms/op
# Warmup Iteration   4: 2,570 ms/op
# Warmup Iteration   5: 1,633 ms/op
Iteration   1: 1,513 ms/op
Iteration   2: 1,839 ms/op
Iteration   3: 2,749 ms/op
Iteration   4: 2,557 ms/op
Iteration   5: 1,929 ms/op
Iteration   6: 2,692 ms/op
Iteration   7: 2,722 ms/op
Iteration   8: 2,627 ms/op
Iteration   9: 2,660 ms/op
Iteration  10: 2,591 ms/op


Result "org.sample.MyBenchmark.parallel_8_thread":
  2,388 ±(99.9%) 0,679 ms/op [Average]
  (min, avg, max) = (1,513, 2,388, 2,749), stdev = 0,449
  CI (99.9%): [1,710, 3,067] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=UTF-8
# Warmup: 5 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.singleThreadImpl

# Run progress: 80,00% complete, ETA 00:02:30
# Fork: 1 of 1
# Warmup Iteration   1: 0,785 ms/op
# Warmup Iteration   2: 0,766 ms/op
# Warmup Iteration   3: 0,762 ms/op
# Warmup Iteration   4: 0,752 ms/op
# Warmup Iteration   5: 0,749 ms/op
Iteration   1: 0,755 ms/op
Iteration   2: 0,814 ms/op
Iteration   3: 0,759 ms/op
Iteration   4: 0,752 ms/op
Iteration   5: 0,762 ms/op
Iteration   6: 0,803 ms/op
Iteration   7: 0,760 ms/op
Iteration   8: 0,765 ms/op
Iteration   9: 0,797 ms/op
Iteration  10: 0,758 ms/op


Result "org.sample.MyBenchmark.singleThreadImpl":
  0,773 ±(99.9%) 0,029 ms/op [Average]
  (min, avg, max) = (0,752, 0,773, 0,814), stdev = 0,021
  CI (99.9%): [0,738, 0,807] (assumes normal distribution)


# Run complete. Total time: 00:12:33

Benchmark                      Mode  Cnt   Score    Error  Units
MyBenchmark.parallel_1_thread  avgt   10   1,981 ±  0,175  ms/op
MyBenchmark.parallel_2_thread  avgt   10   0,606 ±  0,023  ms/op
MyBenchmark.parallel_4_thread  avgt   10   0,145 ±  0,001  ms/op
MyBenchmark.parallel_8_thread  avgt   10   2,388 ±  0,679  ms/op
MyBenchmark.singleThreadImpl   avgt   10   0,773 ±  0,029  ms/op

Process finished with exit code 0