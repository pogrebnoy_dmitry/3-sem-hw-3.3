package org.sample;

public class Pair {
    public long a;
    public long b;

    public Pair (long a, long b) {
        this.a = a;
        this.b = b;
    }

    public Pair add (Pair pair){
        return new Pair(this.a * pair.a, pair.a * this.b + pair.b);
    }
}