package org.sample;

import static java.lang.Math.log;

public class Main {
    public static int COUNT_OF_PAIRS = 8;
    public static int COUNT_OF_THREADS = 4;
    public static int LOG_COT = (int) (log(COUNT_OF_THREADS) / log(2));
    public static int SECTION = Main.COUNT_OF_PAIRS / Main.COUNT_OF_THREADS;
    public static Pair[] pairs = new Pair[COUNT_OF_PAIRS];
    public static Pair result;
    public static Wall wall;
    static Pair[][] pairTable;

    public static void main(String[] args) throws InterruptedException {
        pairs[0] = new Pair(0,1);

        for (int i = 1; i<COUNT_OF_PAIRS; i++){
            pairs[i] = new Pair(i,i);
        }

        pairTable = new Pair[LOG_COT + 1][COUNT_OF_THREADS];


        for (int i = LOG_COT; i >= 0; i--) {
            for (int j = 0; j < COUNT_OF_THREADS; j++) {
                pairTable[i][j] = new Pair(0, 0);
            }
        }

        result = new Pair(0, 0);

        wall = new Wall(COUNT_OF_THREADS);

        for (int i = COUNT_OF_THREADS - 1; i > 0; i--) {
            new PrefixScan(i, pairTable).start();
        }
        PrefixScan last = new PrefixScan(0, pairTable);

        last.start();
        last.join();

        result = pairTable[0][0];
        System.out.println("Result multithreads " + result.b);
        System.out.println("Result singlethread " + singleThreadImpl(pairs));
    }

    public static long singleThreadImpl(Pair[] pairs) {
        long x = 1;
        for (int i = 1; i < COUNT_OF_PAIRS; i++) {
            x = pairs[i].a * x + pairs[i].b;
        }
        return x;
    }
}